run: 
	@go run main.go

push: pull
	@git push origin master

pull:
	@git pull

gen-proto: 
	@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative rpc/qoingrpc/$(name).proto

omitempty:
	@ls ./rpc/qoingrpc/$(file).pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'