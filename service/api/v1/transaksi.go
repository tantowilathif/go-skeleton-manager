package v1

import (
	"context"
	"fmt"

	// "go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Transaksi struct{}

func (*Transaksi) Transaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Transaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTransaksiServiceClient(rpcConn)
	rsp, err := rpcData.Transaksi(context.Background(), &qoingrpc.TransaksiRequest{
		Transaksi: &qoingrpc.Transaksi{
			Tipe:         DataReq.Tipe,
			JumlahMasuk:  float32(DataReq.JumlahMasuk),
			JumlahKeluar: float32(DataReq.JumlahKeluar),
			Tanggal:      DataReq.Tanggal,
			Keterangan:   DataReq.Keterangan,
			KategoriId:   int32(DataReq.KategoriId),
			UserID:       int32(DataReq.UserID),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Transaksi) GetTransaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Transaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTransaksiServiceClient(rpcConn)

	rsp, err := rpcData.GetTransaksi(context.Background(), &qoingrpc.TransaksiRequest{
		Transaksi: &qoingrpc.Transaksi{
			Id:     int32(DataReq.Id),
			UserID: int32(DataReq.UserID),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Transaksi) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Transaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTransaksiServiceClient(rpcConn)

	rsp, err := rpcData.ListTransaksi(context.Background(), &qoingrpc.TransaksiRequest{
		Transaksi: &qoingrpc.Transaksi{
			Id:     int32(DataReq.Id),
			UserID: int32(DataReq.UserID),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Transaksi) EditTransaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Transaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTransaksiServiceClient(rpcConn)
	rsp, err := rpcData.EditTransaksi(context.Background(), &qoingrpc.TransaksiRequest{
		Transaksi: &qoingrpc.Transaksi{
			Id:           int32(DataReq.Id),
			Tipe:         DataReq.Tipe,
			JumlahMasuk:  float32(DataReq.JumlahMasuk),
			JumlahKeluar: float32(DataReq.JumlahKeluar),
			Tanggal:      DataReq.Tanggal,
			Keterangan:   DataReq.Keterangan,
			KategoriId:   int32(DataReq.KategoriId),
			UserID:       int32(DataReq.UserID),
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Transaksi) DeleteTransaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Transaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTransaksiServiceClient(rpcConn)

	rsp, err := rpcData.DeleteTransaksi(context.Background(), &qoingrpc.TransaksiRequest{
		Transaksi: &qoingrpc.Transaksi{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
