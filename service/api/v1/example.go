package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"go-skeleton-manager/rpc/qoinrmq"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Example struct{}

func (*Example) AddExample(c echo.Context) (err error) {

	var call *rpc.Client

	rpcConn, rpcError := call.NewCallGrpc()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewExampleServiceClient(rpcConn)

	rsp, err := rpcUser.GetExamples(context.Background(), &qoingrpc.ExampleRequest{
		Example: &qoingrpc.Example{
			Field1: "Ari Ardiansyah",
			Field2: "ari@loyalto.id",
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return
}
func (*Example) GetExample(c echo.Context) (err error) {
	return err
}
func (*Example) EditExample(c echo.Context) (err error) {
	return err
}
func (*Example) DeleteExample(c echo.Context) (err error) {
	return err
}

func (*Example) SendEmail(c echo.Context) (err error) {

	mailRpc := new(qoinrmq.QoinRmqC)
	mailRpc.Email()

	err = mailRpc.RpcClientOnce(qoinrmq.QoinRmqPayload{
		Param: "this is param",
		Data:  "this is data",
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	return qoinhelper.ResponseContext(200, "success", nil, c)
}
