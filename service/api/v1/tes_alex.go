package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Tesalex struct{}

func (*Tesalex) LaporanTransaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Tesalex)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesalexRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesalexServiceClient(rpcConn)

	rsp, err := rpcData.LapTransaksi(context.Background(), &qoingrpc.TesalexRequest{
		Tesalex: &qoingrpc.Tesalex{
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.LapKeuangan, c)
}
func (*Tesalex) LaporanStokAlex(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.StokAlex)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesalexRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesalexServiceClient(rpcConn)

	rsp, err := rpcData.LaporanStok(context.Background(), &qoingrpc.KartuStokAlexRequest{
		StokAlex: &qoingrpc.KartuStokAlex{
			BarangId:   int32(DataReq.BarangId),
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	//return nil
	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Tesalex) LapRekapStokAlex(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.RekapStokAlex)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesalexRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesalexServiceClient(rpcConn)

	rsp, err := rpcData.LaporanRekapStokAlex(context.Background(), &qoingrpc.RekapStokAlexRequest{
		RekapStokAlex: &qoingrpc.RekapStokAlex{
			BarangId:   DataReq.BarangId,
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
			Tipe: DataReq.Tipe,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	//return nil
	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}
