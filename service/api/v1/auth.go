package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/global"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"

	//echoMidleware "github.com/labstack/echo/v4/middleware"
	"go-skeleton-manager/repository"
	"os"
	"time"
)

type Auth struct{}

func (*Auth) Login(c echo.Context) (err error) {

	var call *rpc.Client

	loginReq := new(repository.Login)
	if err := c.Bind(loginReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	c.Request().ParseForm()

	//fmt.Println(loginReq)
	//loginReq.Username = c.FormValue("username")
	//loginReq.Password = c.FormValue("password")

	//fmt.Printf(string(loginReq.Username), "Username")
	//fmt.Printf(string(loginReq.Email), "Email")
	//fmt.Printf(string(loginReq.Password), "password")

	rpcConn, rpcError := call.NewLoginRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcAuth := qoingrpc.NewAuthServiceClient(rpcConn)

	rsp, err := rpcAuth.Login(context.Background(), &qoingrpc.AuthRequest{
		Auth: &qoingrpc.Auth{
			Id:       loginReq.Id,
			Username: loginReq.Username,
			Email:    loginReq.Email,
			Password: loginReq.Password,
			Token:    "-",
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//JWT := echoMidleware.JWT([]byte(os.Getenv("JWT_KEY")))
	//token, err := CreateToken(12)
	//if err != nil {
	//	c.JSON(http.StatusUnprocessableEntity, err.Error())
	//	return
	//}
	//c.JSON(http.StatusOK, token)

	if rsp.Data == nil {
		return qoinhelper.ResponseContext(400, "Authentication Failed, Username or Email or Password Incorrect", rsp.Data, c)
	} else {

		atClaims := jwt.MapClaims{}
		atClaims["authorized"] = true
		atClaims["exp"] = time.Now().Add(time.Hour * 24).Unix()
		atClaims["users"] = rsp.Data

		at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
		token, err := at.SignedString([]byte(os.Getenv("JWT_KEY")))
		if err != nil {
			qoinhelper.LoggerError(err)
		}

		rt, _ := CreateRefreshToken(rsp.Data.Name)

		(*rsp.Data).Token = token
		(*rsp.Data).RefreshToken = rt

		return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
	}
}

func (*Auth) CekLogin(c echo.Context) (err error) {
	return qoinhelper.ResponseContext(200, "success", "berhasil login", c)
}

func (*Auth) TesRasyidRefresh(c echo.Context) (err error) {
	var call *rpc.Client

	req := new(repository.TesRasyidRefresh)
	if err := c.Bind(req); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	c.Request().ParseForm()

	rpcConn, rpcError := call.NewLoginRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcAuth := qoingrpc.NewAuthServiceClient(rpcConn)

	rsp, err := rpcAuth.TesRasyidRefresh(context.Background(), &qoingrpc.TesRasyidRefreshRequest{
		UserId: req.UserId,
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data == nil {
		return qoinhelper.ResponseContext(400, "Failed, Data Not Found", rsp.Data, c)
	} else {
		token, err := TesRasyidGenToken(rsp.Data)

		if err != nil {
			qoinhelper.LoggerError(err)
		}

		(*rsp.Data).Token = token
		return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
	}
}

func TesRasyidGenToken(userData *qoingrpc.Auth) (string, error) {
	// Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["exp"] = time.Now().Add(time.Minute * 60).Unix()
	atClaims["users"] = userData.Name
	atClaims["sub"] = userData.Id

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("JWT_KEY")))
	if err != nil {
		qoinhelper.LoggerError(err)
	}
	return token, nil
}

func CreateToken(userid uint64) (string, error) {
	var err error
	//Creating Access Token
	//os.Setenv("ACCESS_SECRET", "jdnfksdmfksd") //this should be in an env file
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userid
	atClaims["exp"] = time.Now().Add(time.Minute * 120).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("JWT_KEY")))
	if err != nil {
		return "", err
	}
	return token, nil
}

func CreateRefreshToken(nama string) (string, error) {
	unencoded := strconv.Itoa(int(time.Now().Unix())) + nama
	rt := global.GetMD5Hash(unencoded)
	return rt, nil
}
