package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type TesRasyid struct{}

func (*TesRasyid) Detail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.TesRasyid)
	// fmt.Println(DataReq, "sapi")
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesRasyidRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesRasyidServiceClient(rpcConn)

	rsp, err := rpcData.TesRasyid(context.Background(), &qoingrpc.TesRasyidRequest{
		Data: &qoingrpc.TesRasyid{
			UserId:         int32(DataReq.UserId),
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*TesRasyid) StokList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.TesRasyidStok)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesRasyidRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesRasyidServiceClient(rpcConn)

	rsp, err := rpcData.TesRasyidStokList(context.Background(), &qoingrpc.TesRasyidStokReq{
		Request: &qoingrpc.TesRasyidStok{
			UserId:         int32(DataReq.UserId),
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
			BarangId:       DataReq.BarangId,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*TesRasyid) StokJumlah(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.TesRasyidJumlah)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewTesRasyidRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTesRasyidServiceClient(rpcConn)

	rsp, err := rpcData.TesRasyidJumlahList(context.Background(), &qoingrpc.TesRasyidJumlahReq{
		Request: &qoingrpc.TesRasyidJumlahParam{
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
			Tipe:           DataReq.Tipe,
			BarangId:       DataReq.BarangId,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}
	//rsp.LapKeuangan
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
