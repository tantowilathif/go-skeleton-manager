package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Jabatan struct{}

func (*Jabatan) Jabatan(c echo.Context) (err error) {

	var call *rpc.Client

	jabatanReq := new(repository.Jabatan)
	if err := c.Bind(jabatanReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewJabatanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcJabatan := qoingrpc.NewJabatanServiceClient(rpcConn)

	rsp, err := rpcJabatan.Jabatan(context.Background(), &qoingrpc.JabatanRequest{
		Jabatan: &qoingrpc.Jabatan{
			Id:   int64(jabatanReq.Id),
			Name: jabatanReq.Name,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Jabatan) GetJabatan(c echo.Context) (err error) {
	var call *rpc.Client

	jabatanReq := new(repository.Jabatan)
	if err := c.Bind(jabatanReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewJabatanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcJabatan := qoingrpc.NewJabatanServiceClient(rpcConn)

	rsp, err := rpcJabatan.GetJabatan(context.Background(), &qoingrpc.JabatanRequest{
		Jabatan: &qoingrpc.Jabatan{
			Id:   int64(jabatanReq.Id),
			Name: jabatanReq.Name,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Jabatan) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	jabatanReq := new(repository.Jabatan)
	if err := c.Bind(jabatanReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewJabatanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcJabatan := qoingrpc.NewJabatanServiceClient(rpcConn)

	rsp, err := rpcJabatan.ListJabatan(context.Background(), &qoingrpc.JabatanRequest{
		Jabatan: &qoingrpc.Jabatan{
			Id:   int64(jabatanReq.Id),
			Name: jabatanReq.Name,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Jabatan) EditJabatan(c echo.Context) (err error) {
	var call *rpc.Client

	jabatanReq := new(repository.Jabatan)
	if err := c.Bind(jabatanReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewJabatanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcJabatan := qoingrpc.NewJabatanServiceClient(rpcConn)

	rsp, err := rpcJabatan.EditJabatan(context.Background(), &qoingrpc.JabatanRequest{
		Jabatan: &qoingrpc.Jabatan{
			Id:   int64(jabatanReq.Id),
			Name: jabatanReq.Name,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
		return
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Jabatan) DeleteJabatan(c echo.Context) (err error) {
	return err
}
