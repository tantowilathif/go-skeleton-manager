package v1

import (
	"context"
	"fmt"
	// "go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Pemasukan struct{}

func (*Pemasukan) Pemasukan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pemasukan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPemasukanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPemasukanServiceClient(rpcConn)
	rsp, err := rpcData.Pemasukan(context.Background(), &qoingrpc.PemasukanRequest{
		Pemasukan: &qoingrpc.Pemasukan{
			Jumlah:   float32(DataReq.Jumlah),
			Tanggal: DataReq.Tanggal,
			Keterangan:  DataReq.Keterangan,
			KategoriId: int32(DataReq.KategoriId),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pemasukan) GetPemasukan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pemasukan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPemasukanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPemasukanServiceClient(rpcConn)

	rsp, err := rpcData.GetPemasukan(context.Background(), &qoingrpc.PemasukanRequest{
		Pemasukan: &qoingrpc.Pemasukan{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pemasukan) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pemasukan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPemasukanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPemasukanServiceClient(rpcConn)

	rsp, err := rpcData.ListPemasukan(context.Background(), &qoingrpc.PemasukanRequest{
		Pemasukan: &qoingrpc.Pemasukan{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Pemasukan) EditPemasukan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pemasukan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewPemasukanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPemasukanServiceClient(rpcConn)
	rsp, err := rpcData.EditPemasukan(context.Background(), &qoingrpc.PemasukanRequest{
		Pemasukan: &qoingrpc.Pemasukan{
			Id:        int32(DataReq.Id),
			Jumlah:   float32(DataReq.Jumlah),
			Tanggal: DataReq.Tanggal,
			Keterangan:  DataReq.Keterangan,
			KategoriId: int32(DataReq.KategoriId),
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pemasukan) DeletePemasukan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pemasukan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPemasukanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPemasukanServiceClient(rpcConn)

	rsp, err := rpcData.DeletePemasukan(context.Background(), &qoingrpc.PemasukanRequest{
		Pemasukan: &qoingrpc.Pemasukan{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
