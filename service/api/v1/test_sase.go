package v1

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	//"github.com/labstack/echo/v4"
)

type TestSase struct{}

func (*TestSase) ListTestSase(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.KartuStokSase)
	if err := c.Bind(DataReq); err != nil {
		fmt.Println(err)
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSaseTestRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewTestSaseServiceClient(rpcConn)

	rsp, err := rpcData.TestSase(context.Background(), &qoingrpc.TestSaseRequest{
		TestSase: &qoingrpc.TestSase{
			Id:             int32(DataReq.Id),
			TanggalMulai:   DataReq.TglMulai,
			TanggalSelesai: DataReq.TglSelesai,
			BarangId:       int32(DataReq.BarangId),
			Filter:         DataReq.Filter,
			Offset:         DataReq.Offset,
			Limit:          DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
		fmt.Println("cok", err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}
