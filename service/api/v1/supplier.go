package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Supplier struct{}

func (*Supplier) Supplier(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)
	rsp, err := rpcData.Supplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Nama:   DataReq.Nama,
			Alamat: DataReq.Alamat,
			NoTelp: DataReq.NoTelp,
			Email:  DataReq.Email,
			Detail: qoinhelper.JsonEncode(DataReq.Detail),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Supplier) GetSupplier(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)

	rsp, err := rpcData.GetSupplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	// if rsp.Data != nil {

	// }
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Supplier) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)

	rsp, err := rpcData.ListSupplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Id:     int64(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil || rsp == nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Supplier) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailSupplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.ListDetail, c)
}

func (*Supplier) EditSupplier(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)

	rsp, err := rpcData.EditSupplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Id:     int64(DataReq.Id),
			Nama:   DataReq.Nama,
			Alamat: DataReq.Alamat,
			Email:  DataReq.Email,
			NoTelp: DataReq.NoTelp,
			Detail: qoinhelper.JsonEncode(DataReq.Detail),
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Supplier) DeleteSupplier(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Supplier)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewSupplierRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSupplierServiceClient(rpcConn)

	rsp, err := rpcData.DeleteSupplier(context.Background(), &qoingrpc.SupplierRequest{
		Supplier: &qoingrpc.Supplier{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
