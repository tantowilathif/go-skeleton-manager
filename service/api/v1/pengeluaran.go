package v1

import (
	"context"
	"fmt"
	// "go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Pengeluaran struct{}

func (*Pengeluaran) Pengeluaran(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pengeluaran)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPengeluaranRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPengeluaranServiceClient(rpcConn)
	rsp, err := rpcData.Pengeluaran(context.Background(), &qoingrpc.PengeluaranRequest{
		Pengeluaran: &qoingrpc.Pengeluaran{
			Jumlah:   float32(DataReq.Jumlah),
			Tanggal: DataReq.Tanggal,
			Keterangan:  DataReq.Keterangan,
			KategoriId: int32(DataReq.KategoriId),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pengeluaran) GetPengeluaran(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pengeluaran)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPengeluaranRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPengeluaranServiceClient(rpcConn)

	rsp, err := rpcData.GetPengeluaran(context.Background(), &qoingrpc.PengeluaranRequest{
		Pengeluaran: &qoingrpc.Pengeluaran{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pengeluaran) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pengeluaran)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPengeluaranRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPengeluaranServiceClient(rpcConn)

	rsp, err := rpcData.ListPengeluaran(context.Background(), &qoingrpc.PengeluaranRequest{
		Pengeluaran: &qoingrpc.Pengeluaran{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Pengeluaran) EditPengeluaran(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pengeluaran)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewPengeluaranRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPengeluaranServiceClient(rpcConn)
	rsp, err := rpcData.EditPengeluaran(context.Background(), &qoingrpc.PengeluaranRequest{
		Pengeluaran: &qoingrpc.Pengeluaran{
			Id:        int32(DataReq.Id),
			Jumlah:   float32(DataReq.Jumlah),
			Tanggal: DataReq.Tanggal,
			Keterangan:  DataReq.Keterangan,
			KategoriId: int32(DataReq.KategoriId),
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pengeluaran) DeletePengeluaran(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pengeluaran)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPengeluaranRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPengeluaranServiceClient(rpcConn)

	rsp, err := rpcData.DeletePengeluaran(context.Background(), &qoingrpc.PengeluaranRequest{
		Pengeluaran: &qoingrpc.Pengeluaran{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
