package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type LapTransaksi struct{}

func (*LapTransaksi) LapTransaksi(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.LapTransaksi)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewLapTransaksiRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewLapTransaksiServiceClient(rpcConn)

	rsp, err := rpcData.LaporanTransaksi(context.Background(), &qoingrpc.LapTransaksiRequest{
		LapTransaksi: &qoingrpc.LapFilterTransaksi{
			UserID: int32(DataReq.UserID),
			Startdate: DataReq.StartDate,
			Enddate:   DataReq.EndDate,
		},
	})

	if err != nil || rsp == nil {
		qoinhelper.LoggerError(err)
	}

	fmt.Println(rsp)
	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp, c)
}
