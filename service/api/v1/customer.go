package v1

import (
	"context"
	"fmt"
	// "go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Customer struct{}

func (*Customer) Customer(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.Customer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Nama:      DataReq.Nama,
			NoTelp: DataReq.NoTelp,
			Email: DataReq.Email,
			Alamat: DataReq.Alamat,
			Detail: dataDetail,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Customer) GetCustomer(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)

	rsp, err := rpcData.GetCustomer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Customer) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)

	rsp, err := rpcData.ListCustomer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Customer) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailCustomer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.ListDetail, c)
}

func (*Customer) EditCustomer(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)

	rsp, err := rpcData.EditCustomer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Id:        int32(DataReq.Id),
			Nama:      DataReq.Nama,
			NoTelp: DataReq.NoTelp,
			Email: DataReq.Email,
			Alamat: DataReq.Alamat,
			Detail : dataDetail,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Customer) DeleteCustomer(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Customer)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewCustomerRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewCustomerServiceClient(rpcConn)

	rsp, err := rpcData.DeleteCustomer(context.Background(), &qoingrpc.CustomerRequest{
		Customer: &qoingrpc.Customer{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
