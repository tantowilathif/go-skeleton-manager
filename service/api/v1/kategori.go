package v1

import (
	"context"
	"fmt"
	// "go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Kategori struct{}

func (*Kategori) Kategori(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.Kategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Tipe:      DataReq.Tipe,
			Nama:      DataReq.Nama,
			UserId:    DataReq.UserID,
			Detail: dataDetail,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Kategori) GetKategori(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)

	rsp, err := rpcData.GetKategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Kategori) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)

	rsp, err := rpcData.ListKategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Id:     int32(DataReq.Id),
			UserId: int32(DataReq.UserID),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Kategori) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailKategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Id: int32(DataReq.Id),
			UserId: int32(DataReq.UserID),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp, c)
}

func (*Kategori) EditKategori(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)
	rsp, err := rpcData.EditKategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Id:        int32(DataReq.Id),
			Tipe:      DataReq.Tipe,
			Nama:      DataReq.Nama,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Kategori) DeleteKategori(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Kategori)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewKategoriRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKategoriServiceClient(rpcConn)

	rsp, err := rpcData.DeleteKategori(context.Background(), &qoingrpc.KategoriRequest{
		Kategori: &qoingrpc.Kategori{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
