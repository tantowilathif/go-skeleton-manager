package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Pembelian struct{}

func (*Pembelian) Pembelian(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}
	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.Pembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Tanggal:  DataReq.Tanggal,
			Supplier: DataReq.Supplier,
			Detail:   dataDetail,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pembelian) GetPembelian(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)

	rsp, err := rpcData.GetPembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pembelian) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	//fmt.Println(DataReq)
	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)

	rsp, err := rpcData.ListPembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil || rsp == nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}
func (*Pembelian) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailPembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.ListDetail, c)
}

func (*Pembelian) EditPembelian(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)

	rsp, err := rpcData.EditPembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Id:       int32(DataReq.Id),
			Tanggal:  DataReq.Tanggal,
			Supplier: DataReq.Supplier,
			Detail:   dataDetail,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Pembelian) DeletePembelian(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Pembelian)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPembelianRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPembelianServiceClient(rpcConn)

	rsp, err := rpcData.DeletePembelian(context.Background(), &qoingrpc.PembelianRequest{
		Pembelian: &qoingrpc.Pembelian{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
