package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"os"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
	"github.com/xuri/excelize/v2"
)

type User struct{}

func (*User) User(c echo.Context) (err error) {

	var call *rpc.Client

	userReq := new(repository.User)
	if err := c.Bind(userReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	// path := "192.168.1.48/qoin/img/"
	// path := "D:/Program Files/xampp/htdocs/qoin/img/"
	path := "assets/img/"
	// LoadUrl := "127.0.0.1/qoin/img"
	// fmt.Println(path)
	file, err := global.UploadBase64ToImg(userReq.File, path, userReq.Username)
	if err != nil {
		fmt.Println("alex")
	}
	userReq.Path = path
	userReq.Foto = ""
	if file != nil {
		userReq.Foto = file.(string)
	}
	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewUserServiceClient(rpcConn)

	rsp, err := rpcUser.User(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Name:     userReq.Name,
			Email:    userReq.Email,
			Path:     userReq.Path,
			Foto:     userReq.Foto,
			Username: userReq.Username,
			Password: userReq.Password,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*User) GetUser(c echo.Context) (err error) {
	var call *rpc.Client

	userReq := new(repository.User)
	if err := c.Bind(userReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewUserServiceClient(rpcConn)

	rsp, err := rpcUser.GetUser(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Id: int64(userReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*User) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	userReq := new(repository.User)
	if err := c.Bind(userReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewUserServiceClient(rpcConn)

	rsp, err := rpcUser.ListUser(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Id:     int64(userReq.Id),
			Filter: userReq.Filter,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*User) EditUser(c echo.Context) (err error) {
	var call *rpc.Client

	userReq := new(repository.User)
	if err := c.Bind(userReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewUserServiceClient(rpcConn)

	path := "assets/img/"
	file, err := global.UploadBase64ToImg(userReq.File, path, "user-")

	if err != nil {
		fmt.Println("alex")
	}
	userReq.Path = path
	userReq.Foto = ""
	if file != nil {
		userReq.Foto = file.(string)
	}

	rsp, err := rpcUser.EditUser(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Id:       int64(userReq.Id),
			Name:     userReq.Name,
			Email:    userReq.Email,
			Path:     userReq.Path,
			Foto:     userReq.Foto,
			Username: userReq.Username,
			Password: userReq.Password,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*User) DeleteUser(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.User)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewUserServiceClient(rpcConn)

	rsp, err := rpcData.DeleteUser(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*User) DeleteImage(c echo.Context) (err error) {
	var call *rpc.Client

	userReq := new(repository.User)
	if err := c.Bind(userReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewUserRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcUser := qoingrpc.NewUserServiceClient(rpcConn)

	rsp, err := rpcUser.ListUser(context.Background(), &qoingrpc.UserRequest{
		User: &qoingrpc.User{
			Id:     int64(userReq.Id),
			Filter: userReq.Filter,
		},
	})

	for _, val := range rsp.List {
		os.Remove(val.Path + val.Foto)
	}

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*User) Upload(c echo.Context) (err error) {
	// var call *rpc.Client

	upload, err := c.FormFile("file")
	if err != nil {
		return err
	}

	// Source
	src, err := upload.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	excel, err := excelize.OpenReader(src)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(excel)
	return

}

func (*User) SendEmail(c echo.Context) (err error) {

	email := c.FormValue("email")
	subject := "Hi"
	msg := "<button>Ini Body Massage</button>"

	_, send := global.SendEmail(email, subject, msg)

	if send != nil {
		fmt.Println(send)
	}

	return qoinhelper.ResponseContext(200, "success", "Berhasil Terkirim", c)
}

// func (*User) Compress(c echo.Context) (err error){
// 	image := "user-1638330911.jpg"
// 	height := c.FormValue("height")
// 	width := c.FormValue("width")

// 	compress := global.Compress( image, height, width)

// 	if compress != nil {
// 		fmt.Println(compress)
// 	}

// 	return nil
// }
