package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type KartuStok struct{}

func (*KartuStok) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.KartuStok)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewKartuStokRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewKartuStokServiceClient(rpcConn)

	rsp, err := rpcData.ListKartuStok(context.Background(), &qoingrpc.KartuStokRequest{
		KartuStok: &qoingrpc.KartuStok{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}
