package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Dashboard struct{}

func (*Dashboard) LapKeuangan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Dashboard)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewDashboardRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewDashboardServiceClient(rpcConn)

	rsp, err := rpcData.LapKeuangan(context.Background(), &qoingrpc.DashboardRequest{
		Dashboard: &qoingrpc.Dashboard{
			UserId:         DataReq.UserId,
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.LapKeuangan, c)
}

func (*Dashboard) GetDashboradGrafik(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.DashboardGrafik)

	if err := c.Bind(DataReq); err != nil {
		// fmt.Println(err)
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewDashboardRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewDashboardServiceClient(rpcConn)

	rsp, err := rpcData.DashboardGrafik(context.Background(), &qoingrpc.DashboardGrafikRequest{
		DashboardGrafik: &qoingrpc.DashboardGrafik{
			UserId:         DataReq.UserId,
			Tipe:           DataReq.Tipe,
			TanggalMulai:   DataReq.TanggalMulai,
			TanggalSelesai: DataReq.TanggalSelesai,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
