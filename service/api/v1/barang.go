package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/global"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
	"github.com/xuri/excelize/v2"
)

type Barang struct{}

func (*Barang) Barang(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Barang)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	path := "../img/"
	file, err := global.UploadBase64ToImg(DataReq.File, path, "barang-")
	if err != nil {
	}
	DataReq.Path = path
	DataReq.Foto = ""
	if file != nil {
		DataReq.Foto = file.(string)
	}
	rpcConn, rpcError := call.NewBarangRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBarangServiceClient(rpcConn)
	rsp, err := rpcData.Barang(context.Background(), &qoingrpc.BarangRequest{
		Barang: &qoingrpc.Barang{
			Nama:      DataReq.Nama,
			HargaBeli: DataReq.HargaBeli,
			HargaJual: DataReq.HargaJual,
			Foto:      DataReq.Foto,
			Path:      DataReq.Path,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Barang) GetBarang(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Barang)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewBarangRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBarangServiceClient(rpcConn)

	rsp, err := rpcData.GetBarang(context.Background(), &qoingrpc.BarangRequest{
		Barang: &qoingrpc.Barang{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Barang) Upload(c echo.Context) (err error) {
	// var call *rpc.Client

	upload, err := c.FormFile("file")
	if err != nil {
		return err
	}

	// Source
	src, err := upload.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	excel, err := excelize.OpenReader(src)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(excel)
	return

}

func (*Barang) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Barang)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewBarangRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBarangServiceClient(rpcConn)

	rsp, err := rpcData.ListBarang(context.Background(), &qoingrpc.BarangRequest{
		Barang: &qoingrpc.Barang{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Barang) EditBarang(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Barang)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewBarangRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	path := "../img/"
	file, err := global.UploadBase64ToImg(DataReq.File, path, "barang-")
	if err != nil {
	}
	DataReq.Path = path
	DataReq.Foto = ""
	if file != nil {
		DataReq.Foto = file.(string)
	}
	rpcData := qoingrpc.NewBarangServiceClient(rpcConn)

	rsp, err := rpcData.EditBarang(context.Background(), &qoingrpc.BarangRequest{
		Barang: &qoingrpc.Barang{
			Id:        int32(DataReq.Id),
			Nama:      DataReq.Nama,
			HargaBeli: DataReq.HargaBeli,
			HargaJual: DataReq.HargaJual,
			Foto:      DataReq.Foto,
			Path:      DataReq.Path,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Barang) DeleteBarang(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Barang)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewBarangRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBarangServiceClient(rpcConn)

	rsp, err := rpcData.DeleteBarang(context.Background(), &qoingrpc.BarangRequest{
		Barang: &qoingrpc.Barang{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
