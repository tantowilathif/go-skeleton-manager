package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Bantuan struct{}

func (*Bantuan) Bantuan(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Bantuan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewBantuanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBantuanServiceClient(rpcConn)
	rsp, err := rpcData.Bantuan(context.Background(), &qoingrpc.BantuanRequest{
		Bantuan: &qoingrpc.Bantuan{
			Judul:       DataReq.Judul,
			Keterangan:  DataReq.Keterangan,
			LinkV:       DataReq.LinkV,
			LinkYoutube: DataReq.LinkYoutube,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Bantuan) GetBantuan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Bantuan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewBantuanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBantuanServiceClient(rpcConn)

	rsp, err := rpcData.GetBantuan(context.Background(), &qoingrpc.BantuanRequest{
		Bantuan: &qoingrpc.Bantuan{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Bantuan) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Bantuan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewBantuanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBantuanServiceClient(rpcConn)

	rsp, err := rpcData.ListBantuan(context.Background(), &qoingrpc.BantuanRequest{
		Bantuan: &qoingrpc.Bantuan{
			Id:     int64(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil || rsp == nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Bantuan) EditBantuan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Bantuan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewBantuanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBantuanServiceClient(rpcConn)

	rsp, err := rpcData.EditBantuan(context.Background(), &qoingrpc.BantuanRequest{
		Bantuan: &qoingrpc.Bantuan{
			Id:          int64(DataReq.Id),
			Judul:       DataReq.Judul,
			Keterangan:  DataReq.Keterangan,
			LinkV:       DataReq.LinkV,
			LinkYoutube: DataReq.LinkYoutube,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Bantuan) DeleteBantuan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Bantuan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewBantuanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewBantuanServiceClient(rpcConn)

	rsp, err := rpcData.DeleteBantuan(context.Background(), &qoingrpc.BantuanRequest{
		Bantuan: &qoingrpc.Bantuan{
			Id: int64(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
