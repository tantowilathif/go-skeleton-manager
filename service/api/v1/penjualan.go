package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Penjualan struct{}

func (*Penjualan) Penjualan(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}
	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}
	fmt.Println(DataReq)
	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.Penjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Tanggal:      DataReq.Tanggal,
			TotalHarga:      DataReq.TotalHarga,
			TotalBayar:      DataReq.TotalBayar,
			Kembalian:      DataReq.Kembalian,
			StatusLunas:      DataReq.StatusLunas,
			Detail : dataDetail,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Penjualan) GetPenjualan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)

	rsp, err := rpcData.GetPenjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Penjualan) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)

	rsp, err := rpcData.ListPenjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Id: int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Penjualan) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailPenjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.ListDetail, c)
}

func (*Penjualan) EditPenjualan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.EditPenjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Id:        int32(DataReq.Id),
			Tanggal:      DataReq.Tanggal,
			TotalHarga:      DataReq.TotalHarga,
			TotalBayar:      DataReq.TotalBayar,
			Kembalian:      DataReq.Kembalian,
			StatusLunas:      DataReq.StatusLunas,
			Detail : dataDetail,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Penjualan) DeletePenjualan(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Penjualan)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewPenjualanRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewPenjualanServiceClient(rpcConn)

	rsp, err := rpcData.DeletePenjualan(context.Background(), &qoingrpc.PenjualanRequest{
		Penjualan: &qoingrpc.Penjualan{
			Id: int32(DataReq.Id),

		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}
