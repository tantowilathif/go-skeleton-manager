package v1

import (
	"context"
	"fmt"
	"go-skeleton-manager/repository"
	"go-skeleton-manager/rpc"
	"go-skeleton-manager/rpc/qoingrpc"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/labstack/echo/v4"
)

type Sales struct{}

func (*Sales) Sales(c echo.Context) (err error) {

	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)
	rsp, err := rpcData.Sales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Nama:      DataReq.Nama,
			NoTelp: 	DataReq.NoTelp,
			Email: 		DataReq.Email,
			Alamat: 	DataReq.Alamat,
			Detail: dataDetail,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Sales) GetSales(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)

	rsp, err := rpcData.GetSales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	if rsp.Data != nil {

	}
	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Sales) GetList(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)

	rsp, err := rpcData.ListSales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Id:     int32(DataReq.Id),
			Filter: DataReq.Filter,
			Offset: DataReq.Offset,
			Limit:  DataReq.Limit,
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.List, c)
}

func (*Sales) GetListDetail(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)
	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)

	rsp, err := rpcData.ListDetailSales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.ListDetail, c)
}

func (*Sales) EditSales(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", err.Error(), c)
	}

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)
	var dataDetail = qoinhelper.JsonEncode(DataReq.Detail)

	rsp, err := rpcData.EditSales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Id:        int32(DataReq.Id),
			Nama:      DataReq.Nama,
			NoTelp:  	DataReq.NoTelp,
			Email: 		DataReq.Email,
			Alamat: 	DataReq.Alamat,
			Detail : dataDetail,
		},
	})

	if err != nil {
		fmt.Printf("error while call rpc func %v: ", err)
	}

	if rsp.Code != 200 {
		fmt.Printf("call rpc func failed : %v", rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", rsp.Data, c)
}

func (*Sales) DeleteSales(c echo.Context) (err error) {
	var call *rpc.Client

	DataReq := new(repository.Sales)
	if err := c.Bind(DataReq); err != nil {
		return qoinhelper.ResponseContext(400, "validation", nil, c)
	}

	id, _ := strconv.ParseInt(c.Param("id"), 0, 32)

	DataReq.Id = int32(id)

	rpcConn, rpcError := call.NewSalesRPC()
	if rpcError != nil {
		fmt.Printf("cannot connect to server Rpc : %v", rpcError)
	}

	rpcData := qoingrpc.NewSalesServiceClient(rpcConn)

	rsp, err := rpcData.DeleteSales(context.Background(), &qoingrpc.SalesRequest{
		Sales: &qoingrpc.Sales{
			Id: int32(DataReq.Id),
		},
	})

	if err != nil {
		qoinhelper.LoggerError(err)
	}

	if rsp.Code != 200 {
		qoinhelper.LoggerWarning("call rpc func failed : " + rsp.Message)
	}

	return qoinhelper.ResponseContext(200, "success", "Berhasil Delete", c)
}
