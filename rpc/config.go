package rpc

import (
	grpc "google.golang.org/grpc"
)

// var RpcConfig *QoinRpc

type IRpc interface {
	GrpcStartServer()
	RabbitMQRpcConnection()
}

type QoinRpc struct {
	Host    string
	Port    string
	Rpc     *IRpc
	RServer *grpc.Server
}
