package rpc

import (
	"fmt"
	"go-skeleton-manager/rpc/qoingrpc"
	"log"
	"os"

	grpc "google.golang.org/grpc"
)

type Client struct {
	UserService         *qoingrpc.UserServiceClient
	JabatanService      *qoingrpc.JabatanServiceClient
	ExampleService      *qoingrpc.ExampleServiceClient
	LoginService        *qoingrpc.AuthServiceClient
	BarangService       *qoingrpc.BarangServiceClient
	SupplierService     *qoingrpc.SupplierServiceClient
	SalesService        *qoingrpc.SalesServiceClient
	CutomerService      *qoingrpc.CustomerServiceClient
	PemasukanService    *qoingrpc.PemasukanServiceClient
	PengeluaranService  *qoingrpc.PengeluaranServiceClient
	KategoriService     *qoingrpc.KategoriServiceClient
	TransaksiService    *qoingrpc.TransaksiServiceClient
	KartuStokService    *qoingrpc.KartuStokServiceClient
	DashboardService    *qoingrpc.DashboardServiceClient
	LapTransaksiService *qoingrpc.LapTransaksiServiceClient
	TesalexService      *qoingrpc.TesalexServiceClient
	TestSaseService     *qoingrpc.TestSaseServiceClient
}

func (rc *Client) NewCallGrpc() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewUserRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v",
		os.Getenv("GRPC_HOST"),
		os.Getenv("GRPC_PORT"),
	)
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewExampleRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v",
		os.Getenv("GRPC_HOST"),
		os.Getenv("GRPC_PORT"),
	)
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewLoginRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewJabatanRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}
func (rc *Client) NewBarangRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}
func (rc *Client) NewSupplierRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}
func (rc *Client) NewPembelianRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}
func (rc *Client) NewPenjualanRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewKartuStokRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewCustomerRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewSalesRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewDashboardRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewPemasukanRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewBantuanRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewPengeluaranRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewKategoriRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewTransaksiRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewLapTransaksiRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewTesalexRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewTesRasyidRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}

func (rc *Client) NewSaseTestRPC() (rpcConn *grpc.ClientConn, err error) {
	rpcAddress := fmt.Sprintf("%v:%v", os.Getenv("GRPC_HOST"), os.Getenv("GRPC_PORT"))
	rpcConn, err = grpc.Dial(rpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connection failure : %v", err)
	}
	return rpcConn, err
}
