// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.19.1
// source: rpc/qoingrpc/bantuan.proto

package qoingrpc

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Bantuan struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Judul       string `protobuf:"bytes,2,opt,name=judul,proto3" json:"judul,omitempty"`
	Keterangan  string `protobuf:"bytes,3,opt,name=keterangan,proto3" json:"keterangan,omitempty"`
	LinkV       string `protobuf:"bytes,4,opt,name=link_v,json=linkV,proto3" json:"link_v,omitempty"`
	LinkYoutube string `protobuf:"bytes,5,opt,name=link_youtube,json=linkYoutube,proto3" json:"link_youtube,omitempty"`
	TotalItems  int32  `protobuf:"varint,6,opt,name=total_items,json=totalItems,proto3" json:"total_items,omitempty"`
	Filter      string `protobuf:"bytes,7,opt,name=filter,proto3" json:"filter,omitempty"`
	Offset      int32  `protobuf:"varint,8,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit       int32  `protobuf:"varint,9,opt,name=limit,proto3" json:"limit,omitempty"`
}

func (x *Bantuan) Reset() {
	*x = Bantuan{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Bantuan) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Bantuan) ProtoMessage() {}

func (x *Bantuan) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Bantuan.ProtoReflect.Descriptor instead.
func (*Bantuan) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_bantuan_proto_rawDescGZIP(), []int{0}
}

func (x *Bantuan) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Bantuan) GetJudul() string {
	if x != nil {
		return x.Judul
	}
	return ""
}

func (x *Bantuan) GetKeterangan() string {
	if x != nil {
		return x.Keterangan
	}
	return ""
}

func (x *Bantuan) GetLinkV() string {
	if x != nil {
		return x.LinkV
	}
	return ""
}

func (x *Bantuan) GetLinkYoutube() string {
	if x != nil {
		return x.LinkYoutube
	}
	return ""
}

func (x *Bantuan) GetTotalItems() int32 {
	if x != nil {
		return x.TotalItems
	}
	return 0
}

func (x *Bantuan) GetFilter() string {
	if x != nil {
		return x.Filter
	}
	return ""
}

func (x *Bantuan) GetOffset() int32 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *Bantuan) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

type BantuanList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Limit      int32      `protobuf:"varint,1,opt,name=limit,proto3" json:"limit,omitempty"`
	Offset     int32      `protobuf:"varint,2,opt,name=offset,proto3" json:"offset,omitempty"`
	TotalItems int32      `protobuf:"varint,3,opt,name=total_items,json=totalItems,proto3" json:"total_items,omitempty"`
	List       []*Bantuan `protobuf:"bytes,4,rep,name=list,proto3" json:"list,omitempty"`
}

func (x *BantuanList) Reset() {
	*x = BantuanList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BantuanList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BantuanList) ProtoMessage() {}

func (x *BantuanList) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BantuanList.ProtoReflect.Descriptor instead.
func (*BantuanList) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_bantuan_proto_rawDescGZIP(), []int{1}
}

func (x *BantuanList) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *BantuanList) GetOffset() int32 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *BantuanList) GetTotalItems() int32 {
	if x != nil {
		return x.TotalItems
	}
	return 0
}

func (x *BantuanList) GetList() []*Bantuan {
	if x != nil {
		return x.List
	}
	return nil
}

type BantuanRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Bantuan *Bantuan `protobuf:"bytes,1,opt,name=bantuan,proto3" json:"bantuan,omitempty"`
}

func (x *BantuanRequest) Reset() {
	*x = BantuanRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BantuanRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BantuanRequest) ProtoMessage() {}

func (x *BantuanRequest) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BantuanRequest.ProtoReflect.Descriptor instead.
func (*BantuanRequest) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_bantuan_proto_rawDescGZIP(), []int{2}
}

func (x *BantuanRequest) GetBantuan() *Bantuan {
	if x != nil {
		return x.Bantuan
	}
	return nil
}

type BantuanResponses struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32    `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string   `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	Data    *Bantuan `protobuf:"bytes,3,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *BantuanResponses) Reset() {
	*x = BantuanResponses{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BantuanResponses) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BantuanResponses) ProtoMessage() {}

func (x *BantuanResponses) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BantuanResponses.ProtoReflect.Descriptor instead.
func (*BantuanResponses) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_bantuan_proto_rawDescGZIP(), []int{3}
}

func (x *BantuanResponses) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *BantuanResponses) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *BantuanResponses) GetData() *Bantuan {
	if x != nil {
		return x.Data
	}
	return nil
}

type BantuanListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32        `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string       `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	List    *BantuanList `protobuf:"bytes,3,opt,name=list,proto3" json:"list,omitempty"`
}

func (x *BantuanListResponse) Reset() {
	*x = BantuanListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BantuanListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BantuanListResponse) ProtoMessage() {}

func (x *BantuanListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_bantuan_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BantuanListResponse.ProtoReflect.Descriptor instead.
func (*BantuanListResponse) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_bantuan_proto_rawDescGZIP(), []int{4}
}

func (x *BantuanListResponse) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *BantuanListResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *BantuanListResponse) GetList() *BantuanList {
	if x != nil {
		return x.List
	}
	return nil
}

var File_rpc_qoingrpc_bantuan_proto protoreflect.FileDescriptor

var file_rpc_qoingrpc_bantuan_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x72, 0x70, 0x63, 0x2f, 0x71, 0x6f, 0x69, 0x6e, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x62,
	0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x62, 0x61,
	0x6e, 0x74, 0x75, 0x61, 0x6e, 0x22, 0xf0, 0x01, 0x0a, 0x07, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61,
	0x6e, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x14, 0x0a, 0x05, 0x6a, 0x75, 0x64, 0x75, 0x6c, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x6a, 0x75, 0x64, 0x75, 0x6c, 0x12, 0x1e, 0x0a, 0x0a, 0x6b, 0x65, 0x74, 0x65, 0x72,
	0x61, 0x6e, 0x67, 0x61, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6b, 0x65, 0x74,
	0x65, 0x72, 0x61, 0x6e, 0x67, 0x61, 0x6e, 0x12, 0x15, 0x0a, 0x06, 0x6c, 0x69, 0x6e, 0x6b, 0x5f,
	0x76, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x69, 0x6e, 0x6b, 0x56, 0x12, 0x21,
	0x0a, 0x0c, 0x6c, 0x69, 0x6e, 0x6b, 0x5f, 0x79, 0x6f, 0x75, 0x74, 0x75, 0x62, 0x65, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x6c, 0x69, 0x6e, 0x6b, 0x59, 0x6f, 0x75, 0x74, 0x75, 0x62,
	0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x69, 0x74, 0x65, 0x6d, 0x73,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x49, 0x74, 0x65,
	0x6d, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66,
	0x66, 0x73, 0x65, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73,
	0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x22, 0x82, 0x01, 0x0a, 0x0b, 0x42, 0x61, 0x6e,
	0x74, 0x75, 0x61, 0x6e, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69,
	0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16,
	0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06,
	0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f,
	0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x74, 0x6f, 0x74,
	0x61, 0x6c, 0x49, 0x74, 0x65, 0x6d, 0x73, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x69, 0x73, 0x74, 0x18,
	0x04, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x10, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e,
	0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x04, 0x6c, 0x69, 0x73, 0x74, 0x22, 0x3c, 0x0a,
	0x0e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x2a, 0x0a, 0x07, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x10, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75,
	0x61, 0x6e, 0x52, 0x07, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x22, 0x66, 0x0a, 0x10, 0x42,
	0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x12,
	0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63,
	0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x24, 0x0a,
	0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x10, 0x2e, 0x62, 0x61,
	0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x04, 0x64,
	0x61, 0x74, 0x61, 0x22, 0x6d, 0x0a, 0x13, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x4c, 0x69,
	0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18,
	0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x28, 0x0a, 0x04, 0x6c, 0x69, 0x73, 0x74,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e,
	0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x04, 0x6c, 0x69,
	0x73, 0x74, 0x32, 0xe9, 0x02, 0x0a, 0x0e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3f, 0x0a, 0x07, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e,
	0x12, 0x17, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75,
	0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x62, 0x61, 0x6e, 0x74,
	0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x42, 0x0a, 0x0a, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e,
	0x74, 0x75, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42,
	0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e,
	0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x46, 0x0a, 0x0b, 0x4c, 0x69,
	0x73, 0x74, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x62, 0x61, 0x6e, 0x74,
	0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x1c, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e,
	0x74, 0x75, 0x61, 0x6e, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x12, 0x43, 0x0a, 0x0b, 0x45, 0x64, 0x69, 0x74, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61,
	0x6e, 0x12, 0x17, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74,
	0x75, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x62, 0x61, 0x6e,
	0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x45, 0x0a, 0x0d, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75,
	0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x19, 0x2e, 0x62, 0x61, 0x6e, 0x74, 0x75, 0x61, 0x6e, 0x2e, 0x42, 0x61, 0x6e, 0x74,
	0x75, 0x61, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x42, 0x0e,
	0x5a, 0x0c, 0x72, 0x70, 0x63, 0x2f, 0x71, 0x6f, 0x69, 0x6e, 0x67, 0x72, 0x70, 0x63, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_rpc_qoingrpc_bantuan_proto_rawDescOnce sync.Once
	file_rpc_qoingrpc_bantuan_proto_rawDescData = file_rpc_qoingrpc_bantuan_proto_rawDesc
)

func file_rpc_qoingrpc_bantuan_proto_rawDescGZIP() []byte {
	file_rpc_qoingrpc_bantuan_proto_rawDescOnce.Do(func() {
		file_rpc_qoingrpc_bantuan_proto_rawDescData = protoimpl.X.CompressGZIP(file_rpc_qoingrpc_bantuan_proto_rawDescData)
	})
	return file_rpc_qoingrpc_bantuan_proto_rawDescData
}

var file_rpc_qoingrpc_bantuan_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_rpc_qoingrpc_bantuan_proto_goTypes = []interface{}{
	(*Bantuan)(nil),             // 0: bantuan.Bantuan
	(*BantuanList)(nil),         // 1: bantuan.BantuanList
	(*BantuanRequest)(nil),      // 2: bantuan.BantuanRequest
	(*BantuanResponses)(nil),    // 3: bantuan.BantuanResponses
	(*BantuanListResponse)(nil), // 4: bantuan.BantuanListResponse
}
var file_rpc_qoingrpc_bantuan_proto_depIdxs = []int32{
	0, // 0: bantuan.BantuanList.list:type_name -> bantuan.Bantuan
	0, // 1: bantuan.BantuanRequest.bantuan:type_name -> bantuan.Bantuan
	0, // 2: bantuan.BantuanResponses.data:type_name -> bantuan.Bantuan
	1, // 3: bantuan.BantuanListResponse.list:type_name -> bantuan.BantuanList
	2, // 4: bantuan.BantuanService.Bantuan:input_type -> bantuan.BantuanRequest
	2, // 5: bantuan.BantuanService.GetBantuan:input_type -> bantuan.BantuanRequest
	2, // 6: bantuan.BantuanService.ListBantuan:input_type -> bantuan.BantuanRequest
	2, // 7: bantuan.BantuanService.EditBantuan:input_type -> bantuan.BantuanRequest
	2, // 8: bantuan.BantuanService.DeleteBantuan:input_type -> bantuan.BantuanRequest
	3, // 9: bantuan.BantuanService.Bantuan:output_type -> bantuan.BantuanResponses
	3, // 10: bantuan.BantuanService.GetBantuan:output_type -> bantuan.BantuanResponses
	4, // 11: bantuan.BantuanService.ListBantuan:output_type -> bantuan.BantuanListResponse
	3, // 12: bantuan.BantuanService.EditBantuan:output_type -> bantuan.BantuanResponses
	3, // 13: bantuan.BantuanService.DeleteBantuan:output_type -> bantuan.BantuanResponses
	9, // [9:14] is the sub-list for method output_type
	4, // [4:9] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_rpc_qoingrpc_bantuan_proto_init() }
func file_rpc_qoingrpc_bantuan_proto_init() {
	if File_rpc_qoingrpc_bantuan_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_rpc_qoingrpc_bantuan_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Bantuan); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_bantuan_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BantuanList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_bantuan_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BantuanRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_bantuan_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BantuanResponses); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_bantuan_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BantuanListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_rpc_qoingrpc_bantuan_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_rpc_qoingrpc_bantuan_proto_goTypes,
		DependencyIndexes: file_rpc_qoingrpc_bantuan_proto_depIdxs,
		MessageInfos:      file_rpc_qoingrpc_bantuan_proto_msgTypes,
	}.Build()
	File_rpc_qoingrpc_bantuan_proto = out.File
	file_rpc_qoingrpc_bantuan_proto_rawDesc = nil
	file_rpc_qoingrpc_bantuan_proto_goTypes = nil
	file_rpc_qoingrpc_bantuan_proto_depIdxs = nil
}
