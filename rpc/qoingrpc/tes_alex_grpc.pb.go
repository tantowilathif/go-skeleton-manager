// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package qoingrpc

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// TesalexServiceClient is the client API for TesalexService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type TesalexServiceClient interface {
	// Unary
	ListTesalex(ctx context.Context, in *TesalexRequest, opts ...grpc.CallOption) (*TesalexListResponse, error)
	LapTransaksi(ctx context.Context, in *TesalexRequest, opts ...grpc.CallOption) (*TesalexListResponse, error)
	LaporanStok(ctx context.Context, in *KartuStokAlexRequest, opts ...grpc.CallOption) (*LaporanStokListResponse, error)
	LaporanRekapStokAlex(ctx context.Context, in *RekapStokAlexRequest, opts ...grpc.CallOption) (*ResultRekapStokAlexResponse, error)
}

type tesalexServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewTesalexServiceClient(cc grpc.ClientConnInterface) TesalexServiceClient {
	return &tesalexServiceClient{cc}
}

func (c *tesalexServiceClient) ListTesalex(ctx context.Context, in *TesalexRequest, opts ...grpc.CallOption) (*TesalexListResponse, error) {
	out := new(TesalexListResponse)
	err := c.cc.Invoke(ctx, "/tesalex.TesalexService/ListTesalex", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tesalexServiceClient) LapTransaksi(ctx context.Context, in *TesalexRequest, opts ...grpc.CallOption) (*TesalexListResponse, error) {
	out := new(TesalexListResponse)
	err := c.cc.Invoke(ctx, "/tesalex.TesalexService/LapTransaksi", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tesalexServiceClient) LaporanStok(ctx context.Context, in *KartuStokAlexRequest, opts ...grpc.CallOption) (*LaporanStokListResponse, error) {
	out := new(LaporanStokListResponse)
	err := c.cc.Invoke(ctx, "/tesalex.TesalexService/LaporanStok", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tesalexServiceClient) LaporanRekapStokAlex(ctx context.Context, in *RekapStokAlexRequest, opts ...grpc.CallOption) (*ResultRekapStokAlexResponse, error) {
	out := new(ResultRekapStokAlexResponse)
	err := c.cc.Invoke(ctx, "/tesalex.TesalexService/LaporanRekapStokAlex", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// TesalexServiceServer is the server API for TesalexService service.
// All implementations must embed UnimplementedTesalexServiceServer
// for forward compatibility
type TesalexServiceServer interface {
	// Unary
	ListTesalex(context.Context, *TesalexRequest) (*TesalexListResponse, error)
	LapTransaksi(context.Context, *TesalexRequest) (*TesalexListResponse, error)
	LaporanStok(context.Context, *KartuStokAlexRequest) (*LaporanStokListResponse, error)
	LaporanRekapStokAlex(context.Context, *RekapStokAlexRequest) (*ResultRekapStokAlexResponse, error)
	mustEmbedUnimplementedTesalexServiceServer()
}

// UnimplementedTesalexServiceServer must be embedded to have forward compatible implementations.
type UnimplementedTesalexServiceServer struct {
}

func (UnimplementedTesalexServiceServer) ListTesalex(context.Context, *TesalexRequest) (*TesalexListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListTesalex not implemented")
}
func (UnimplementedTesalexServiceServer) LapTransaksi(context.Context, *TesalexRequest) (*TesalexListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LapTransaksi not implemented")
}
func (UnimplementedTesalexServiceServer) LaporanStok(context.Context, *KartuStokAlexRequest) (*LaporanStokListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LaporanStok not implemented")
}
func (UnimplementedTesalexServiceServer) LaporanRekapStokAlex(context.Context, *RekapStokAlexRequest) (*ResultRekapStokAlexResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LaporanRekapStokAlex not implemented")
}
func (UnimplementedTesalexServiceServer) mustEmbedUnimplementedTesalexServiceServer() {}

// UnsafeTesalexServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to TesalexServiceServer will
// result in compilation errors.
type UnsafeTesalexServiceServer interface {
	mustEmbedUnimplementedTesalexServiceServer()
}

func RegisterTesalexServiceServer(s grpc.ServiceRegistrar, srv TesalexServiceServer) {
	s.RegisterService(&TesalexService_ServiceDesc, srv)
}

func _TesalexService_ListTesalex_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TesalexRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TesalexServiceServer).ListTesalex(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tesalex.TesalexService/ListTesalex",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TesalexServiceServer).ListTesalex(ctx, req.(*TesalexRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TesalexService_LapTransaksi_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TesalexRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TesalexServiceServer).LapTransaksi(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tesalex.TesalexService/LapTransaksi",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TesalexServiceServer).LapTransaksi(ctx, req.(*TesalexRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TesalexService_LaporanStok_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(KartuStokAlexRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TesalexServiceServer).LaporanStok(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tesalex.TesalexService/LaporanStok",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TesalexServiceServer).LaporanStok(ctx, req.(*KartuStokAlexRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TesalexService_LaporanRekapStokAlex_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RekapStokAlexRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TesalexServiceServer).LaporanRekapStokAlex(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tesalex.TesalexService/LaporanRekapStokAlex",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TesalexServiceServer).LaporanRekapStokAlex(ctx, req.(*RekapStokAlexRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// TesalexService_ServiceDesc is the grpc.ServiceDesc for TesalexService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var TesalexService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "tesalex.TesalexService",
	HandlerType: (*TesalexServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListTesalex",
			Handler:    _TesalexService_ListTesalex_Handler,
		},
		{
			MethodName: "LapTransaksi",
			Handler:    _TesalexService_LapTransaksi_Handler,
		},
		{
			MethodName: "LaporanStok",
			Handler:    _TesalexService_LaporanStok_Handler,
		},
		{
			MethodName: "LaporanRekapStokAlex",
			Handler:    _TesalexService_LaporanRekapStokAlex_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "rpc/qoingrpc/tes_alex.proto",
}
