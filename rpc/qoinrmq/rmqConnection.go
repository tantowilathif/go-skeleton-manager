package qoinrmq

import (
	"fmt"
	"os"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"

	"github.com/streadway/amqp"
)

var QoinRmq *QoinRmqC

type QoinRmqPayload struct {
	Param interface{} `json:"param"`
	Data  interface{} `json:"data"`
}

type QoinRmqC struct {
	ConnectionUrl string
	Connection    *amqp.Connection
	Channel       *amqp.Channel
	Queue         amqp.Queue
	MainQue       string
	PrefetchCount int
	PrefetchSize  int
	Error         error
	BodyRequest   *QoinRmqPayload
	Response      QoinRmqResponse
}

type QoinRmqConnUrl struct {
	Host, Port, Username, Password, VirtualHost, QueueName string
}

type QoinRmqResponse struct {
	Code    int         `json:"code"`
	Status  interface{} `json:"status"`
	Message interface{} `json:"message"`
	Data    interface{} `json:"data"`
}

func (conn *QoinRmqC) StartConnection() {
	conn.Connection, conn.Error = amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s%s",
		os.Getenv("QOINRMQ_USER"),
		os.Getenv("QOINRMQ_PASS"),
		os.Getenv("QOINRMQ_HOST"),
		os.Getenv("QOINRMQ_PORT"),
		os.Getenv("QOINRMQ_VHOST"),
	))

	qoinhelper.LoggerError(conn.Error) //Print if error given

	conn.PrefetchCount, _ = strconv.Atoi(os.Getenv("QOINRMQ_CONC_COUNT")) // for concurrency handling count
	conn.PrefetchSize, _ = strconv.Atoi(os.Getenv("QOINRMQ_CONC_SIZE"))   // for concurrency handling size

	qoinhelper.LoggerInfo("Create Connection Success") // Print success connection
}

func (conn *QoinRmqC) Email() {

	conn.MainQue = os.Getenv("QOINRMQ_QUEUE")
	conn.ConnectionUrl = fmt.Sprintf("amqp://%s:%s@%s:%s%s",
		os.Getenv("QOINRMQ_USER"),
		os.Getenv("QOINRMQ_PASS"),
		os.Getenv("QOINRMQ_HOST"),
		os.Getenv("QOINRMQ_PORT"),
		os.Getenv("QOINRMQ_VHOST"),
	)
}

//Add this function for use another queue
// func (conn *QoinRmqC) OtherQueue() {

// 	conn.MainQue = os.Getenv("QOINRMQ_QUEUE_OTHER")
// 	conn.ConnectionUrl = fmt.Sprintf("amqp://%s:%s@%s:%s%s",
// 		os.Getenv("QOINRMQ_USER"),
// 		os.Getenv("QOINRMQ_PASS"),
// 		os.Getenv("QOINRMQ_HOST"),
// 		os.Getenv("QOINRMQ_PORT"),
// 		os.Getenv("QOINRMQ_VHOST"),
// 	)
// }
