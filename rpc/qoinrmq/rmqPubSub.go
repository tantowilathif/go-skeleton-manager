package qoinrmq

import (
	"encoding/json"
	"os"
	"strconv"
	"time"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"

	"github.com/streadway/amqp"
)

func (rpc *QoinRmqC) RpcClient(body interface{}) (response QoinRmqResponse, err error) {

	rpc.StartConnection()

	if rpc.Connection.IsClosed() {
		rpc.StartConnection()
	}

	connClient := rpc.Connection
	defer connClient.Close()

	chanClient, err := connClient.Channel()
	if err != nil {
		qoinhelper.LoggerError(err)
	}

	defer chanClient.Close()
	quenameRd := os.Getenv("QOINRMQ_PREFIX") + strconv.Itoa(int(time.Now().UnixNano()))

	queClient, err := chanClient.QueueDeclare(
		quenameRd,
		true,
		true,
		false,
		false,
		nil,
	)
	if err != nil {
		qoinhelper.LoggerError(err)
	}

	corrId := qoinhelper.UUID()

	_, errMainDeclare := chanClient.QueueDeclare(
		rpc.MainQue,
		true,
		false,
		false,
		false,
		nil,
	)
	qoinhelper.LoggerError(errMainDeclare)

	err = chanClient.Publish(
		"",
		rpc.MainQue,
		false,
		false,
		amqp.Publishing{
			ContentType:   "text/plain",
			CorrelationId: corrId,
			ReplyTo:       queClient.Name,
			Body:          []byte(qoinhelper.JsonEncode(body)),
			Expiration:    "60000",
		},
	)

	message, err := chanClient.Consume(
		queClient.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		if connClient.IsClosed() {
			return rpc.RpcClient(body)
		}
		qoinhelper.LoggerWarning("Error Consume : " + strconv.FormatBool(connClient.IsClosed()) + err.Error())
	}
	qoinhelper.LoggerError(err)

	timeout, _ := strconv.Atoi(os.Getenv("QOINRMQ_TIMEOUT"))
	rpcTimeout := time.After(time.Duration(timeout) * time.Second)
	wait := true

	for wait {

		select {
		case <-rpcTimeout:
			response.Code = 500
			response.Status = "error"
			response.Message = "RPC timeout " + rpc.MainQue
			wait = false

		case data := <-message:
			if corrId == data.CorrelationId {
				if qoinhelper.JsonEncode(body) == string(data.Body) {
					response.Code = 400
					response.Status = "failed"
					response.Message = "Rpc server not respond"
				} else {
					err = json.Unmarshal(data.Body, &response)
					if err != nil {
						response.Code = 500
						response.Status = "failed"
						response.Message = "failed binding response"
						response.Data = "failed binding response"
						qoinhelper.LoggerError(err)
					}
				}
				wait = false
			}
		}
	}
	return
}

func (rpc *QoinRmqC) RpcClientOnce(body interface{}) error {

	rpc.StartConnection()

	if rpc.Connection.IsClosed() {
		rpc.StartConnection()
	}

	connClient := rpc.Connection
	defer connClient.Close()

	chanClient, err := connClient.Channel()
	if err != nil {
		qoinhelper.LoggerError(err)
	}

	defer chanClient.Close()

	qName, err := chanClient.QueueDeclare(
		rpc.MainQue, // name
		true,        // durable
		false,       // delete when unused
		false,       // exclusive
		false,       // no-wait
		nil,         // arguments
	)

	if err != nil {
		qoinhelper.LoggerError(err)
	}
	qoinhelper.LoggerInfo("Queue Once : " + qName.Name)

	err = chanClient.Publish(
		"",
		qName.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/json",
			Body:        []byte(qoinhelper.JsonEncode(body)),
			Expiration:  "60000",
		},
	)
	if err != nil {
		qoinhelper.LoggerError(err)
	}

	return err
}
