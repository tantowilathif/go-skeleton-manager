module go-skeleton-manager

go 1.16

require (
	//bitbucket.org/loyaltoid/go-helpers v0.0.1
	github.com/Qoin-Digital-Indonesia/qoingohelper v1.0.1
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/h2non/bimg v1.1.5
	github.com/joho/godotenv v1.4.0
	github.com/labstack/echo/v4 v4.6.1
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/swaggo/echo-swagger v1.1.4
	github.com/xuri/excelize/v2 v2.4.1
	golang.org/x/tools v0.1.7 // indirect
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
