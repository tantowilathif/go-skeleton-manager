package global

import (
	"crypto/md5"
	b64 "encoding/base64"
	"encoding/hex"
	"fmt"
	"image/jpeg"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/nfnt/resize"
	"gopkg.in/gomail.v2"
)

func UploadImg(c echo.Context, param string, path string) (data interface{}, err error) {
	// Read form fields
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.Mkdir(path, 0777)

		if err != nil {
			fmt.Println("tidak bisa create")
		}
	}
	file, err := c.FormFile(param)
	if err != nil {
		return nil, err
	}
	src, err := file.Open()
	if err != nil {
		return nil, err
	}
	defer src.Close()

	// Destination
	dst, err := os.Create(path + file.Filename)
	if err != nil {
		return nil, err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return nil, err
	}

	return file.Filename, err
}

func getExtention(base64 string) string {
	png := strings.Contains(base64, "image/png")
	jpeg := strings.Contains(base64, "image/jpeg")
	gif := strings.Contains(base64, "image/gif")

	ex := ".jpg"

	if png {
		ex = ".png"
	} else if jpeg {
		ex = ".jpeg"
	} else if gif {
		ex = ".gif"
	}

	return ex
}

// param width 700 height 700
func UploadBase64ToImg(param string, path string, name string) (data interface{}, err error) {
	// Read form fields
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, 0755)

		if err != nil {
			fmt.Println("tidak bisa create")
		}
	}

	if param != "" {
		b64data := param[strings.IndexByte(param, ',')+1:]
		dec, err := b64.StdEncoding.DecodeString(b64data)
		if err != nil {
			return data, err
		}

		time := time.Now().Unix()
		//fmt.Println(nm, "alex")s
		nmfile := name + strconv.Itoa(int(time)) + getExtention(param)
		f, err := os.Create(path + nmfile)
		if err != nil {
			return data, err
		}
		defer f.Close()

		if _, err := f.Write(dec); err != nil {
			return data, err
		}
		if err := f.Sync(); err != nil {
			return data, err
		}
		fmt.Println(nmfile)
		Compress(path+nmfile, "0", "700")
		fmt.Println("Berhasil Compress")
		return nmfile, err
	}
	fmt.Println("alex2")
	return data, err
}

func SendEmail(email string, subject string, msg string) (data string, err error) {

	Email := os.Getenv("SMTP_EMAIL")
	Password := os.Getenv("SMTP_PASSWORD")
	Host := "smtp.gmail.com"
	Port := 587

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", Email)
	mailer.SetHeader("To", email)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", msg)
	mailer.Attach("../img/barang-1638329840.jpg")

	dialer := gomail.NewDialer(
		Host,
		Port,
		Email,
		Password,
	)

	abc := dialer.DialAndSend(mailer)
	if abc != nil {
		log.Fatal(err.Error())
	}

	log.Println("Mail sent!")
	return data, err
}

func Compress(nmfile string, height string, width string) error {
	file, err := os.Open(nmfile)
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	hg, err := strconv.Atoi(height)
	uhg := uint(hg)

	wd, err := strconv.Atoi(width)
	uwd := uint(wd)

	// (width, height, input file, kernel sampling)
	m := resize.Resize(uwd, uhg, img, resize.Lanczos3)

	out, err := os.Create(nmfile)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	fmt.Println("Ini Masuk Compress")
	return jpeg.Encode(out, m, nil)
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
