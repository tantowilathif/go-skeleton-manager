package routes

import (
	// "go-skeleton-manager/routes/middleware"

	middleware "go-skeleton-manager/routes/middleware"
	v1 "go-skeleton-manager/service/api/v1"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	echoMidleware "github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger" // echo-swagger
)

// @title Swagger Go Skeleton Manager
// @version 1.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name Ari Ardiansyah
// @contact.url http://www.qoin.id
// @contact.email dev@qoin.id

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /v1
func Router() {
	routing := echo.New()
	// routing.Validator = &models.CustomValidator{Validator: validator.New()}

	routing.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowCredentials: true,
		AllowMethods:     []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))

	group := routing.Group("/api/v1")

	Auth := echoMidleware.JWT([]byte(os.Getenv("JWT_KEY")))

	//Sales
	sales := new(v1.Sales)
	group.POST("/sales", sales.Sales)
	group.GET("/sales", sales.GetSales)
	group.GET("/sales/list", sales.GetList)
	group.GET("/sales/listDetail/:id", sales.GetListDetail)
	group.PUT("/sales", sales.EditSales)
	group.DELETE("/sales/:id", sales.DeleteSales)

	//USER
	auth := new(v1.Auth)
	group.POST("/login", auth.Login)
	group.POST("/cekLogin", auth.CekLogin, Auth)

	//CUSTOMER
	customer := new(v1.Customer)
	group.POST("/customer", customer.Customer)
	group.GET("/customer", customer.GetCustomer)
	group.GET("/customer/list", customer.GetList)
	group.GET("/customer/listDetail/:id", customer.GetListDetail)
	group.PUT("/customer", customer.EditCustomer)
	group.DELETE("/customer/:id", customer.DeleteCustomer)

	//KETEGORI
	kategori := new(v1.Kategori)
	group.POST("/kategori", kategori.Kategori)
	group.GET("/kategori", kategori.GetKategori)
	group.GET("/kategori/list", kategori.GetList)
	group.GET("/kategori/listDetail/:id", kategori.GetListDetail)
	group.PUT("/kategori", kategori.EditKategori)
	group.DELETE("/kategori/:id", kategori.DeleteKategori)

	//PEMASUKAN
	pemasukan := new(v1.Pemasukan)
	group.POST("/pemasukan", pemasukan.Pemasukan)
	group.GET("/pemasukan", pemasukan.GetPemasukan)
	group.GET("/pemasukan/list", pemasukan.GetList)
	group.PUT("/pemasukan", pemasukan.EditPemasukan)
	group.DELETE("/pemasukan/:id", pemasukan.DeletePemasukan)

	//PENGELUARAN
	pengeluaran := new(v1.Pengeluaran)
	group.POST("/pengeluaran", pengeluaran.Pengeluaran)
	group.GET("/pengeluaran", pengeluaran.GetPengeluaran)
	group.GET("/pengeluaran/list", pengeluaran.GetList)
	group.PUT("/pengeluaran", pengeluaran.EditPengeluaran)
	group.DELETE("/pengeluaran/:id", pengeluaran.DeletePengeluaran)

	//TRANSAKSI
	transaksi := new(v1.Transaksi)
	group.POST("/transaksi", transaksi.Transaksi)
	group.GET("/transaksi", transaksi.GetTransaksi)
	group.GET("/transaksi/list", transaksi.GetList)
	group.PUT("/transaksi", transaksi.EditTransaksi)
	group.DELETE("/transaksi/:id", transaksi.DeleteTransaksi)

	//BANTUAN
	bantuan := new(v1.Bantuan)
	group.POST("/bantuan", bantuan.Bantuan)
	group.GET("/bantuan", bantuan.GetBantuan)
	group.GET("/bantuan/list", bantuan.GetList)
	group.PUT("/bantuan", bantuan.EditBantuan)
	group.DELETE("/bantuan/:id", bantuan.DeleteBantuan)

	//LAPTRANSAKSI
	laptransaksi := new(v1.LapTransaksi)
	group.GET("/laptransaksi", laptransaksi.LapTransaksi)
	// group.GET("/bantuan", bantuan.GetBantuan)

	//USER
	user := new(v1.User)
	group.POST("/user", user.User)
	group.GET("/user/:id", user.GetUser)
	group.GET("/user/list", user.GetList)
	group.PUT("/user", user.EditUser)
	group.DELETE("/user/:id", user.DeleteUser)
	group.PUT("/userimage", user.DeleteImage)
	group.POST("/sendemail", user.SendEmail)
	// group.POST("/compress", user.Compress)

	//BARANG
	barang := new(v1.Barang)
	group.GET("/barang/list", barang.GetList)
	group.PUT("/barang", barang.EditBarang)
	group.DELETE("/barang/:id", barang.DeleteBarang)
	group.POST("/barang", barang.Barang)
	group.POST("/barang/upload", barang.Upload)

	//Pembelian
	pembelian := new(v1.Pembelian)
	group.GET("/pembelian/list", pembelian.GetList)
	group.GET("/pembelian/listDetail/:id", pembelian.GetListDetail)
	group.PUT("/pembelian", pembelian.EditPembelian)
	group.DELETE("/pembelian/:id", pembelian.DeletePembelian)
	group.POST("/pembelian", pembelian.Pembelian)

	//Penjualan
	penjualan := new(v1.Penjualan)
	group.GET("/penjualan/list", penjualan.GetList)
	group.GET("/penjualan/listDetail/:id", penjualan.GetListDetail)
	group.PUT("/penjualan", penjualan.EditPenjualan)
	group.DELETE("/penjualan/:id", penjualan.DeletePenjualan)
	group.POST("/penjualan", penjualan.Penjualan)

	//Supplier
	supplier := new(v1.Supplier)
	group.GET("/supplier/list", supplier.GetList)
	group.GET("/supplier/listDetail/:id", supplier.GetListDetail)
	group.PUT("/supplier", supplier.EditSupplier)
	group.DELETE("/supplier/:id", supplier.DeleteSupplier)
	group.POST("/supplier", supplier.Supplier)

	//KartuStok
	kartuStok := new(v1.KartuStok)
	group.GET("/kartustok", kartuStok.GetList)

	//Dashboard
	dashboard := new(v1.Dashboard)
	group.GET("/dashboard/laporan_keuangan", dashboard.LapKeuangan)
	group.GET("/dashboard/grafik_garis", dashboard.GetDashboradGrafik)

	tes_alex := new(v1.Tesalex)
	group.GET("/tes-alex/laporan", tes_alex.LaporanTransaksi)
	group.GET("/tes-alex/rekap-stok", tes_alex.LapRekapStokAlex)
	group.GET("/tes-alex/laporan-stok", tes_alex.LaporanStokAlex)

	tes_rasyid := new(v1.TesRasyid)
	group.GET("/tes-rasyid/detail", tes_rasyid.Detail)
	group.GET("/tes-rasyid/stok", tes_rasyid.StokList)
	group.GET("/tes-rasyid/stokjumlah", tes_rasyid.StokJumlah)
	group.POST("/tes-rasyid/refresh", auth.TesRasyidRefresh)

	//BARANG
	jabatan := new(v1.Jabatan)
	group.GET("/jabatan/list", jabatan.GetList)
	group.PUT("/jabatan", jabatan.EditJabatan)

	// TEST ASSET
	group.Static("/assets/img", "assets/img")

	//Sample for gRpc with Http
	example := new(v1.Example)                            // Http with gRpc
	group.POST("/example", example.AddExample, Auth)      // Http with gRpc
	group.GET("/example", example.GetExample, Auth)       // Http with gRpc
	group.PUT("/example", example.EditExample, Auth)      // Http with gRpc
	group.DELETE("/example", example.DeleteExample, Auth) // Http with gRpc

	//Sample for Queue Messaging with Rabbit
	group.GET("/example/email", example.SendEmail, Auth) // Http with RabbitRpc

	group.GET("/swagger/*", echoSwagger.WrapHandler)

	// routing.HTTPErrorHandler = middleware.RouteHandler
	routing.Logger.Fatal(routing.Start(":" + os.Getenv("APP_PORT")))
}
