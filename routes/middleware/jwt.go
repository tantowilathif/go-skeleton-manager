package middleware

import (
	"fmt"
	"os"
	"strings"

	qoinhelpers "github.com/Qoin-Digital-Indonesia/qoingohelper"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

var Unauthorized = map[string]interface{}{
	"message": "unauthorized",
	"code":    401,
	"status":  "unauthorized",
}

var SecretKey map[string]interface{}

func init() {
	SecretKey = map[string]interface{}{
		"develop":    "5559aQxWShCqHS1A6WdSdT84kE7UXxAC",
		"staging":    "iGSpuiPe6YubU7TRu0NZ8dHBUutMIXZvn194MHyfhFwQJ4VTNYNn1qErusfNdgTD",
		"production": "u99h2cjaWB3ccUT4zc1R0vPCCs8ijAca1Xe9MeojG8D0fngedI1ECy62ZLUilfCM",
	}
}

func Permission(permissionKey string, c echo.Context) (ok bool, cl jwt.MapClaims) {
	authToken := c.Request().Header.Get("Authorization")
	auths := strings.Split(authToken, " ")

	if len(auths) < 1 {
		return false, nil
	}

	token, err := jwt.Parse(auths[1], func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return false, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(SecretKey[os.Getenv("APP_MODE")].(string)), nil
	})

	if err != nil {
		qoinhelpers.LoggerInfo(err.Error())
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {
		return checkPermission(claims, permissionKey)
	}

	return false, nil
}

func checkPermission(claim jwt.MapClaims, permissionKey string) (ok bool, cl jwt.MapClaims) {
	return qoinhelpers.InArray(permissionKey, claim["Permission"]), claim
}

func GetPermission(keys string, c echo.Context) interface{} {
	authToken := c.Request().Header.Get("Authorization")
	auths := strings.Split(authToken, " ")

	if len(auths) < 1 {
		return false
	}

	token, err := jwt.Parse(auths[1], func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return false, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(SecretKey[os.Getenv("APP_MODE")].(string)), nil
	})

	if err != nil {
		qoinhelpers.LoggerInfo(err.Error())
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {
		return claims[keys]
	}

	return nil
}
