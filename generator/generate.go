package main

import (
	"fmt"
	"os"
)

func main() {

	param := os.Args[1:]

	fmt.Println(param)

	f, err := os.Create("rpc/qoingrpc/" + param[0] + ".proto")
	if err != nil {
		fmt.Println(err)
		return
	}
	d := []string{
		`syntax = "proto3";`,
		``,
		`package ` + param[0] + `;`,
		`option go_package="rpc/qoingrpc";`,
		``,
		`message Example {`,
		`	int64 Id = 1;`,
		`	string Field1 = 2;`,
		`	string Field2 = 3;`,
		``,
		`	message ChildExample {`,
		`		int32 Id = 1;`,
		`		string ChildField1 = 2;`,
		`		string ChildField2 = 3;`,
		`	}`,
		`}`,
		``,
		`message ExampleRequest {`,
		`	Example example = 1;`,
		`}`,
		``,
		`message ExampleResponses {`,
		`	int32 code = 1;`,
		`	string message = 2;`,
		`	string data = 3;`,
		`}`,
		``,
		`service ExampleService{`,
		``,
		`	// Unary`,
		`	rpc GetExamples(ExampleRequest) returns (ExampleResponses){};`,
		`	rpc PostExamples(ExampleRequest) returns (ExampleResponses){};`,
		`	rpc EditExamples(ExampleRequest) returns (ExampleResponses){};`,
		`	rpc DeleteExamples(ExampleRequest) returns (ExampleResponses){};`,
		`}`,
	}
	for _, v := range d {
		fmt.Fprintln(f, v)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("file written successfully")
}
