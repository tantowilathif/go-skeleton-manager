package repository

type LapTransaksi struct {
	UserID    int32  `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	StartDate string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	EndDate   string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}
