package repository

type KartuStok struct {
	Id           int32   `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Keterangan   string  `param:"keterangan" query:"keterangan" form:"keterangan" json:"keterangan" xml:"keterangan"`
	ReffType     string  `param:"reff_type" query:"reff_type" form:"reff_type" json:"reff_type" xml:"reff_type"`
	ReffId       int32   `param:"reff_id" query:"reff_id" form:"reff_id" json:"reff_id" xml:"reff_id"`
	BarangId     int32   `param:"barang_id" query:"barang_id" form:"barang_id" json:"barang_id" xml:"barang_id"`
	JenisStok    string  `param:"jenis_stok" query:"jenis_stok" form:"jenis_stok" json:"jenis_stok" xml:"jenis_stok"`
	JumlahMasuk  int32   `param:"jumlah_masuk" query:"jumlah_masuk" form:"jumlah_masuk" json:"jumlah_masuk" xml:"jumlah_masuk"`
	HargaMasuk   float32 `param:"harga_masuk" query:"harga_masuk" form:"harga_masuk" json:"harga_masuk" xml:"harga_masuk"`
	JumlahKeluar int32   `param:"jumlah_keluar" query:"jumlah_keluar" form:"jumlah_keluar" json:"jumlah_keluar" xml:"jumlah_keluar"`
	HargaKeluar  float32 `param:"harga_keluar" query:"harga_keluar" form:"harga_keluar" json:"harga_keluar" xml:"harga_keluar"`
	CreatedAt    string  `param:"created_at" query:"created_at" form:"created_at" json:"created_at" xml:"created_at"`
	Filter       string  `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset       int32   `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit        int32   `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
