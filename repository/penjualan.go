package repository

type Penjualan struct {
	Id       int32      `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Tanggal  string 	`param:"tanggal" query:"tanggal" form:"tanggal" json:"tanggal" xml:"tanggal"`
	TotalHarga float32  `param:"total_harga" query:"total_harga" form:"total_harga" json:"total_harga" xml:"total_harga"`
	TotalBayar float32  `param:"total_bayar" query:"total_bayar" form:"total_bayar" json:"total_bayar" xml:"total_bayar"`
	Kembalian float32   `param:"kembalian" query:"kembalian" form:"kembalian" json:"kembalian" xml:"kembalian"`
	StatusLunas string  `param:"status_lunas" query:"status_lunas" form:"status_lunas" json:"status_lunas" xml:"status_lunas"`
	Detail []map[string]interface{}   `param:"detail" query:"detail" form:"detail" json:"detail" xml:"detail"`
	Filter    string  `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset    int32   `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit     int32   `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
