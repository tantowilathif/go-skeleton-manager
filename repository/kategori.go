package repository

type Kategori struct {
	Id     int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Tipe string  `param:"tipe" query:"tipe" form:"tipe" json:"tipe" xml:"tipe"`
	Nama   string `param:"nama" query:"nama" form:"nama" json:"nama" xml:"nama"`
	UserID   int32 `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	Detail []map[string]interface{} `param:"detail" query:"detail" form:"detail" json:"detail" xml:"detail"`
	Filter string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit  int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
