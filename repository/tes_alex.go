package repository

type Tesalex struct {
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}
type StokAlex struct {
	BarangId   int32 `param:"barang_id" query:"barang_id" form:"barang_id" json:"barang_id" xml:"barang_id"`
	TanggalMulai   string `param:"barang_id" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}

type RekapStokAlex struct {
	BarangId   string `param:"barang_id" query:"barang_id" form:"barang_id" json:"barang_id" xml:"barang_id"`
	TanggalMulai   string `param:"barang_id" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
	Tipe string `param:"tipe" query:"tipe" form:"tipe" json:"tipe" xml:"tipe"`
}