package repository

type Supplier struct {
	Id     int32                    `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Nama   string                   `param:"nama" query:"nama" form:"nama" json:"nama" xml:"nama"`
	Email  string                   `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	NoTelp string                   `param:"no_telp" query:"no_telp" form:"no_telp" json:"no_telp" xml:"no_telp"`
	Alamat string                   `param:"alamat" query:"alamat" form:"alamat" json:"alamat" xml:"alamat"`
	Filter string                   `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset int32                    `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit  int32                    `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
	Detail []map[string]interface{} `param:"detail" query:"detail" form:"detail" json:"detail" xml:"detail"`
}
