package repository

type Barang struct {
	Id        int32   `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Nama      string  `param:"nama" query:"nama" form:"nama" json:"nama" xml:"nama"`
	HargaBeli float32 `param:"harga_beli" query:"harga_beli" form:"harga_beli" json:"harga_beli" xml:"harga_beli"`
	HargaJual float32 `param:"harga_jual" query:"harga_jual" form:"harga_jual" json:"harga_jual" xml:"harga_jual"`
	Foto      string  `param:"foto" query:"foto" form:"foto" json:"foto" xml:"foto"`
	File      string  `param:"file" query:"file" form:"file" json:"file" xml:"file"`
	Path      string  `param:"path" query:"path" form:"path" json:"path" xml:"path"`
	Filter    string  `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset    int32   `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit     int32   `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
