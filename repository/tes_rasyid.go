package repository

type TesRasyid struct {
	UserId         int64  `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}

type TesRasyidStok struct {
	UserId         int64  `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	BarangId       int32  `param:"barang_id" query:"barang_id" form:"barang_id" json:"barang_id" xml:"barang_id"`
	Offset         int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit          int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}

type TesRasyidJumlah struct {
	BarangId       string `param:"barang_id" query:"barang_id" form:"barang_id" json:"barang_id" xml:"barang_id"`
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
	Tipe           string `param:"tipe" query:"tipe" form:"tipe" json:"tipe" xml:"tipe"`
}

type TesRasyidRefresh struct {
	UserId int32 `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
}
