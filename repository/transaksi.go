package repository

type Transaksi struct {
	Id     int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Tipe   string `param:"tipe" query:"tipe" form:"tipe" json:"tipe" xml:"tipe"`
	JumlahMasuk   float32 `param:"jumlah_masuk" query:"jumlah_masuk" form:"jumlah_masuk" json:"jumlah_masuk" xml:"jumlah_masuk"`
	JumlahKeluar   float32 `param:"jumlah_keluar" query:"jumlah_keluar" form:"jumlah_keluar" json:"jumlah_keluar" xml:"jumlah_keluar"`
	Tanggal string  `param:"tanggal" query:"tanggal" form:"tanggal" json:"tanggal" xml:"tanggal"`
	Keterangan  string `param:"keterangan" query:"keterangan" form:"keterangan" json:"keterangan" xml:"keterangan"`
	KategoriId int32 `param:"kategori_id" query:"kategori_id" form:"kategori_id" json:"kategori_id" xml:"kategori_id"`
	UserID int32 `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	Filter string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit  int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
