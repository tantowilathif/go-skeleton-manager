package repository

type Bantuan struct {
	Id          int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Judul       string `param:"judul" query:"judul" form:"judul" json:"judul" xml:"judul"`
	Keterangan  string `param:"keterangan" query:"keterangan" form:"keterangan" json:"keterangan" xml:"keterangan"`
	LinkV       string `param:"link_v" query:"link_v" form:"link_v" json:"link_v" xml:"link_v"`
	LinkYoutube string `param:"link_youtube" query:"link_youtube" form:"link_youtube" json:"link_youtube" xml:"link_youtube"`
	Filter      string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset      int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit       int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
