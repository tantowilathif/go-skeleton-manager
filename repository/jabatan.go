package repository

type Jabatan struct {
	Id   int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	ID   int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name string `param:"name" query:"name" form:"name" json:"name" xml:"name"`
}
