package repository

type User struct {
	Id       int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name     string `param:"name" query:"name" form:"name" json:"name" xml:"name"`
	Email    string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Path     string `param:"path" query:"path" form:"path" json:"path" xml:"path"`
	Foto     string `param:"foto" query:"foto" form:"foto" json:"foto" xml:"foto"`
	File     string `param:"file" query:"file" form:"file" json:"file" xml:"file"`
	Username string `param:"username" query:"username" form:"username" json:"username" xml:"username"`
	Password string `param:"password" query:"password" form:"password" json:"password" xml:"password"`
	Filter   string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
}

type UserReset struct {
	Id       int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Password string `param:"password" query:"password" form:"password" json:"password" xml:"password"`
}
