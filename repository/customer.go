package repository

type Customer struct {
	Id     int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Nama   string `param:"nama" query:"nama" form:"nama" json:"nama" xml:"nama"`
	NoTelp int64  `param:"no_telp" query:"no_telp" form:"no_telp" json:"no_telp" xml:"no_telp"`
	Email  string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Alamat string `param:"alamat" query:"alamat" form:"alamat" json:"alamat" xml:"alamat"`
	Detail []map[string]interface{} `param:"detail" query:"detail" form:"detail" json:"detail" xml:"detail"`
	Filter string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit  int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
