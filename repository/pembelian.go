package repository

type Pembelian struct {
	Id       int32     `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Tanggal  string 	`param:"tanggal" query:"tanggal" form:"tanggal" json:"tanggal" xml:"tanggal" validate:"required,tanggal"`
	Supplier string    `param:"supplier" query:"supplier" form:"supplier" json:"supplier" xml:"supplier"`
	Detail []map[string]interface{}   `param:"detail" query:"detail" form:"detail" json:"detail" xml:"detail"`
	Filter    string  `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset    int32   `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit     int32   `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}