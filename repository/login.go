package repository

type Login struct {
	Id       int64  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username string `param:"username" query:"username" form:"username" json:"username" xml:"username"`
	Email    string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Password string `param:"password" query:"password" form:"password" json:"password" xml:"password"`
}
