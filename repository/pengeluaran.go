package repository

type Pengeluaran struct {
	Id     int32  `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Jumlah   float32 `param:"jumlah" query:"jumlah" form:"jumlah" json:"jumlah" xml:"jumlah"`
	Tanggal string  `param:"tanggal" query:"tanggal" form:"tanggal" json:"tanggal" xml:"tanggal"`
	Keterangan  string `param:"keterangan" query:"keterangan" form:"keterangan" json:"keterangan" xml:"keterangan"`
	KategoriId int32 `param:"kategori_id" query:"kategori_id" form:"kategori_id" json:"kategori_id" xml:"kategori_id"`
	Filter string `param:"filter" query:"filter" form:"filter" json:"filter" xml:"filter"`
	Offset int32  `param:"offset" query:"offset" form:"offset" json:"offset" xml:"offset"`
	Limit  int32  `param:"limit" query:"limit" form:"limit" json:"limit" xml:"limit"`
}
