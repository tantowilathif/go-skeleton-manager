package repository

type Dashboard struct {
	UserId         int64  `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}

type DashboardGrafik struct {
	UserId         int64  `param:"user_id" query:"user_id" form:"user_id" json:"user_id" xml:"user_id"`
	Tipe           string `param:"tipe" query:"tipe" form:"tipe" json:"tipe" xml:"tipe"`
	TanggalMulai   string `param:"tanggal_mulai" query:"tanggal_mulai" form:"tanggal_mulai" json:"tanggal_mulai" xml:"tanggal_mulai"`
	TanggalSelesai string `param:"tanggal_selesai" query:"tanggal_selesai" form:"tanggal_selesai" json:"tanggal_selesai" xml:"tanggal_selesai"`
}
