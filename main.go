package main

import (
	"go-skeleton-manager/routes"
	"log"

	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Printf("Error while get environment file %v", err)
	}

	routes.Router() //Initialization routes http
}
