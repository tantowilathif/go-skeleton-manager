# Go Skeleton Manager

[![logo](https://avatars.githubusercontent.com/u/72009988?s=200&v=4)](https://www.qoin.id/)

# Introduction
This repo for create new services for backend manager, and there is two protocol to be use.

# Installation


use the skeleton for make new service for manager

via SSH
```git
git clone git@bitbucket.org:loyaltoid/go-skeleton-manager.git
```
via HTTPS
```git
git clone https://username@bitbucket.org/loyaltoid/go-skeleton-manager.git
```


# Directory

* repository 
  - logic.go
* routes
  - middleware 
     ` this dir for middleware (jwt, signature, etc). `
  - api.go ` this dir for create middleware.`
* rpc
  - qoingrpc
    `this dir include protobuff file.`
  - qoinrmq
    `include logic helper and config for rabbitmqrpc / messaging / queue.`
  - client.go `function for create new connection for gRpc / RabbitmqRpc.`
  - config.go `interface for structure gRpc / RabbitmqRpc.`
* service
    `this dir for logic handler`


# Usage
Create proto file in qoingrpc dir and run this command for generate pub
```shell
protoc rpc/qoingrpc/sample.proto --go_out=plugins=grpc:. 
```

### Routes Gateway
#### Create New Routes
overwrite api.go in routes dir 
```golang
func Router() {
    ...
	
    sample := new(v1.Sample)
	group.POST("/sample", sample.HandlerFuncPost, Midlleware)
	group.GET("/user", sample.HandlerFuncGet, Auth)
	group.PUT("/user", sample.HandlerFuncPut, Auth)
	group.DELETE("/user", sample.HandlerFuncDelete, Auth)

    ...
}
```

```golang
package main

import (
    ...
	"go-skeleton-manager/routes"
	...
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Printf("Error while get environment file %v", err)
	}

	routes.Router()
}

```


